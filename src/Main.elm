import Browser
import Html exposing (Html, button, div, text, ul, li, h3, p, textarea, input)
import Html.Attributes exposing (value)
import Html.Events exposing (onClick, onInput)

import Markdown


main =
    Browser.sandbox { init = initModel, update = update, view = view}


type Msg = AddSubItem ItemId
    | TogglExpanded ItemId
    | ToggleIsInEditMode ItemId
    | UpdateItemNote ItemId String
    | UpdateItemTitle ItemId String
    | ToggleIsInTitleEditMode ItemId


type alias ItemId = Int

type alias ItemModel =
    { id: ItemId
    , title: String
    , contents: String
    , expanded: Bool
    , parentId: ItemId
    , isInEditMode: Bool
    , isInTitleEditMode: Bool
    }

type alias Model = List ItemModel


rootItem : ItemModel
rootItem = 
    { id = 0
    , title = "Root Node"
    , contents = ""
    , expanded = True
    , parentId = -1
    , isInEditMode = False
    , isInTitleEditMode = False
    }


initModel : List ItemModel
initModel = [rootItem]


update : Msg -> Model -> Model
update msg model =
    case msg of
        AddSubItem rootItemId ->
            let
                itemId = getNextId model
                newItem = 
                    { id = itemId
                    , title = "New Item #" ++ String.fromInt(itemId)
                    , contents = ""
                    , expanded = False
                    , parentId = rootItemId
                    , isInEditMode = False
                    , isInTitleEditMode = False
                    }
            in
                newItem :: model
                    -- expand parent while adding new items to it
                    |> List.map ( \item -> if item.id == rootItemId then { item | expanded = True } else item )
        TogglExpanded itemId ->
            model
                |> List.map ( \item -> if item.id == itemId then { item | expanded = not item.expanded } else item )
        ToggleIsInEditMode itemId ->
            model
                |> List.map ( \item -> if item.id == itemId then { item | isInEditMode = not item.isInEditMode } else item )
        UpdateItemNote itemId val ->
            model
                |> List.map ( \item -> if item.id == itemId then { item | contents = val } else item )
        UpdateItemTitle itemId val ->
            model
                |> List.map ( \item -> if item.id == itemId then { item | title = val } else item )
        ToggleIsInTitleEditMode itemId ->
            model
                |> List.map ( \item -> if item.id == itemId then { item | isInTitleEditMode = not item.isInTitleEditMode } else item )



getChildren : ItemId -> List ItemModel -> List ItemModel
getChildren itemId items =
    List.filter (\item -> item.parentId == itemId) items


getNextId : List ItemModel -> ItemId
getNextId items =
    let
        usedIds = 
            items
                |> List.map (\item -> item.id)
                |> List.maximum
    in
        case usedIds of
            Nothing ->
                rootItem.id + 1
            Just maxId ->
                maxId + 1


itemNoteInputView : ItemId -> String -> Html Msg
itemNoteInputView itemId itemContents =
    div [] 
        [ button [ onClick (ToggleIsInEditMode itemId) ] [ text "Done" ]
        , textarea 
            [ value itemContents 
            , onInput (UpdateItemNote itemId)
            ] []
        , div [] <| (Markdown.toHtml Nothing itemContents)
        ]


itemTitleView : ItemModel -> Html Msg
itemTitleView item =
    div [] 
        ( 
            if item.isInTitleEditMode then
                [ input 
                    [ value item.title 
                    , onInput (UpdateItemTitle item.id)
                    ] []
                , button [ onClick (ToggleIsInTitleEditMode item.id) ] [ text "Done" ]
                ]
            else
                [ h3 [] [ text item.title ]
                , button [ onClick (ToggleIsInTitleEditMode item.id) ] [ text "Edit Title" ]
                ]
        )


treeView : List ItemModel -> ItemModel -> Html Msg
treeView items root =
    let
        childrenItems = getChildren root.id items
        childrenView = List.map (treeView items) childrenItems
        hasChildren = (List.length childrenItems) /= 0
        isChildrenVisible = hasChildren && root.expanded
        expandText = if root.expanded then "Collapse" else "Expand"
        isExpandable = hasChildren && root.id /= 0
    in
        li []
            [ (itemTitleView root)
            , button [ onClick (AddSubItem root.id) ] [ text "Add child item" ]
            , (if isExpandable then button [ onClick (TogglExpanded root.id) ] [ text expandText ] else text "")
            , (if root.isInEditMode then (itemNoteInputView root.id root.contents ) else button [ onClick (ToggleIsInEditMode root.id) ] [ text "Edit Note" ]) 
            , (if root.isInEditMode then text "" else div [] <| Markdown.toHtml Nothing root.contents)
            , (if isChildrenVisible then ul [] childrenView else text "")
            ]



view : Model -> Html Msg
view model =
    div []
        [ ul []
            [ treeView model rootItem ]
        ]
